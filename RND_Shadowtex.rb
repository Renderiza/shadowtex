=begin
(c) 2013, Renderiza Studio

Permission to use, copy, modify, and distribute this software for 
any purpose and without fee is hereby granted.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE.

== Information
Author:: Renderiza 
Name:: Shadowtex
Version:: 1.0.1
SU Version:: 2013
Date:: 6/11/2013
Description:: Paints all faces that are in shadow.
History::
1.0.0 :: 
	* Original release. 
1.0.1 :: 
	* Only one click needed to undo all painted faces.	
	* Shortened some code lines to iterate through faces.
=end

module RND_Extensions

	module RND_Shadow_Facer
		require 'sketchup.rb'
		require 'extensions.rb'

		unless file_loaded?( __FILE__ )
			file_loaded( __FILE__ )
			# Create the extension.
			ext = SketchupExtension.new 'Shadowtex', 'RND_Shadowtex/RND_Shadowtex_menus.rb'
			
			# Attach some nice info.
			ext.creator     = 'Renderiza'
			ext.version     = '1.0.1'
			ext.copyright   = '2013, Renderiza Studio.'
			ext.description = 'Paints all faces that are in shadow'

			# Register and load the extension on startup.
			Sketchup.register_extension ext, true
		end
		
	end
	
end

file_loaded( __FILE__ )