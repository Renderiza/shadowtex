=begin
(c) 2013, Renderiza Studio

Permission to use, copy, modify, and distribute this software for 
any purpose and without fee is hereby granted.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE.

== Information
Author:: Renderiza 
Name:: Shadowtex
Version:: 1.0.1
SU Version:: 2013
Date:: 6/11/2013
Description:: Paints all faces that are in shadow.
History::
1.0.0 :: 
	* Original release.  
1.0.1 :: 
	* Only one click needed to undo all painted faces.
	* Shortened some code lines to iterate through faces.
=end

module RND_Extensions

	module RND_Shadowtex
		require 'sketchup.rb'
		require 'RND_Shadowtex/RND_Shadowtex_loader.rb'
	end

	if !file_loaded?('rnd_menu_loader')
		@@rnd_tools_menu = UI.menu("Plugins").add_submenu("Renderiza Tools")
	end
	
	#------New menu Items---------------------------
	if !file_loaded?('rnd_ew_loader')
		@@rnd_ew_menu = @@rnd_tools_menu.add_submenu("Websites")
		@@rnd_ew_menu.add_separator
		@@rnd_ew_menu.add_item("PluginStore"){UI.openURL('http://sketchucation.com/resources/pluginstore?pauthor=renderiza')}
		@@rnd_ew_menu.add_item("Extension Warehouse"){UI.openURL('http://extensions.sketchup.com/en/user/5451/store')}
		@@rnd_tools_menu.add_separator
		@@rnd_ew_menu.add_separator
	end
	#------------------------------------------------
	if !file_loaded?(__FILE__) then
		@@rnd_tools_menu.add_item('Shadowtex') {
		Sketchup.active_model.select_tool RND_Extensions::RND_Shadowtex::Shadowtex.new ; 
		}
		# Add toolbar
		rnd_shadowtex_tb = UI::Toolbar.new "Shadowtex" ;
		rnd_shadowtex_cmd = UI::Command.new("Shadowtex") {
		Sketchup.active_model.select_tool RND_Extensions::RND_Shadowtex::Shadowtex.new
		}
		rnd_shadowtex_cmd.small_icon = "img/rnd_shadowtex_1_16.png"
		rnd_shadowtex_cmd.large_icon = "img/rnd_shadowtex_1_24.png"
		rnd_shadowtex_cmd.tooltip = "Shadowtex"
		rnd_shadowtex_cmd.status_bar_text = " Texture Faces in Shadows"
		rnd_shadowtex_cmd.menu_text = "Shadowtex"
		rnd_shadowtex_tb = rnd_shadowtex_tb.add_item rnd_shadowtex_cmd
		rnd_shadowtex_tb.show
	end
	
	file_loaded('rnd_ew_loader')
	file_loaded('rnd_menu_loader')
	file_loaded(__FILE__)
end