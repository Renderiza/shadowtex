=begin
(c) 2013, Renderiza Studio

Permission to use, copy, modify, and distribute this software for 
any purpose and without fee is hereby granted.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE.

== Information
Author:: Renderiza 
Name:: Shadowtex
Version:: 1.0.1
SU Version:: 2013
Date:: 6/11/2013
Description:: Paints all faces that are in shadow.
History::
1.0.0 :: 
	* Original release.  
1.0.1 :: 
	* Only one click needed to undo all painted faces.
	* Shortened some code lines to iterate through faces.
=end

module RND_Extensions

	module RND_Shadowtex

		class Shadowtex
		
			@@model = Sketchup.active_model
			@@path = File.dirname(__FILE__) # this plugin's folder
			@@rnd_shadowtex = "rnd_shadowtex.html"
			@@shadowtex_files = File.join(@@path, '/ShadowtexFiles/')
			@@shadowtex_dlg = nil

			
			def initialize()
			
				@shadowtex_1_file =  File.join(@@path, @@rnd_shadowtex)
		   
				if (!@shadowtex_1_file) then 
					UI.messagebox("Cannot find file '#{@@rnd_shadowtex} in folder '#{@@path}'")
					return
				end
				
				if @@shadowtex_dlg == nil
					@@shadowtex_dlg = UI::WebDialog.new("Shadowtex v.1.0.1", false, "Shadowtex v.1.0.1", 335, 570, 70, 95, true)

					@@shadowtex_dlg.add_action_callback("push_frame") do |d,p| 
					push_frame(d,p)
					end
				end
				
				@@shadowtex_dlg.set_size(335, 570)
				@@shadowtex_dlg.set_file(@shadowtex_1_file)
				@@shadowtex_dlg.show()
				
			end #def 
			
			
			
			def push_frame(dialog,data)
					
				params = query_to_hash(data) # parse state information 
				
				#----------------------------	
				#Set:: File	
				#----------------------------
				if params['file'].to_s == "1"
					get_file(title = "Set:: FILE")

					file = 0
					script = "top.file = " + file.to_s + ";"
					dialog.execute_script(script)
				end
				
				#----------------------------					
				#Click Run
				#----------------------------	
				if params['run'].to_s == "1" 	
					Sketchup.active_model.select_tool RND_Extensions::RND_Shadowtex::Shadowtex.new
					
					@r = params['r'].to_i
					@g = params['g'].to_i
					@b = params['b'].to_i
					
					#----------------------------	
					#Set:: Size	
					#----------------------------
					@getsize = params['getsize'].to_f
				
					### 
					model = Sketchup.active_model
					ents = model.active_entities
					faces = ents.grep(Sketchup::Face)
					materials = model.materials
					m = materials.add "shadow"
					if @file
						texture = m.texture = @file   
						m.texture.size = @getsize
					end
					v_sun = model.shadow_info["SunDirection"]
					
					###
					model.start_operation("Shadowtex", true)
					faces.each do |e|

						boundbox = e.bounds
						center = boundbox.center
						hit_path = Sketchup.active_model.raytest [center,v_sun]

						if hit_path != nil
						
							if params['pick'].to_s == "tex" 
								e.material = m
							else
								e.material = [@r,@g,@b]
							end
						
						end
						
					end
					model.commit_operation
					###
					
					run = 0
					script = "top.run = " + run.to_s + ";"
					dialog.execute_script(script)
					Sketchup.send_action('selectSelectionTool:') #Selection Tool
				end #run
				
				#----------------------------	
				#Click Done
				#----------------------------	
				if params['done'].to_s == "1"
					Sketchup.send_action('selectSelectionTool:') #Selection Tool
					@@shadowtex_dlg.close
				end
				
			end #def
			
			
			
			def get_file(title = "Choose a file")
				filepath = UI.openpanel( title, "")
				if filepath
					@file = filepath.gsub("\\",'//')
					else
					return nil
				end
			end #def
			
			
			
			def unescape(string)
				if string != nil
					string = string.gsub(/\+/, ' ').gsub(/((?:%[0-9a-fA-F]{2})+)/n) do
						[$1.delete('%')].pack('H*')
					end
				end
				return string
			end #def
			
			
			
			def query_to_hash(query)
				param_pairs = query.split('&')
				param_hash = {}
				for param in param_pairs
					name, value = param.split('=')
					name = unescape(name)
					value = unescape(value)
					param_hash[name] = value
				end
				return param_hash
			end #def
					
		end #class
		
	end #module

end #module

file_loaded( File.basename(__FILE__) )   # Mark the script loaded. 